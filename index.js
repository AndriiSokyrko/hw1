const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();
app.use(morgan('common', {
  stream: fs.createWriteStream('./access.log', {flags: 'a'})
}));

app.use(morgan('dev'));
const { filesRouter } = require('./filesRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    res.status(500).send({"message":"Server error"})
  }
}

start();

//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.error('err')
  res.status(500).send({'message': 'Server error'});
}
