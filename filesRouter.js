const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile ,putFile, deleteFile} = require('./filesService.js');

router.post('/', createFile);

router.get('/', getFiles);

router.get('/:filename', getFile);

router.put('/:filename', putFile);

router.delete('/:filename', deleteFile);

router.get("*", function(req,res){
  res.status(400).send({"message": "Server error"});
});

// Other endpoints - put, delete.

module.exports = {
  filesRouter: router
};
