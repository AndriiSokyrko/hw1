const fs = require('fs')
const path = require('path')

function createPasswordFile(res,filename,password){
    const passwords = path.resolve(__dirname,'filePassword.json')
    let fileExists = fs.existsSync(passwords)
    if (typeof password==='undefined' ) {
        return true
    }
    if (password.trim()==='' ) {
        res.status(400).send({"message": "Password can't be empty"});
        return false
    } else {
        if (!fileExists) {
            if(typeof password ==='undefined'){
                return true
            } else {
                let contentObj = {}
                contentObj = Object.assign({}, {filename: password})
                const modiObj = JSON.stringify(contentObj)
                fs.writeFile(passwords, modiObj, (err) => {
                    if (err) {
                        res.status(400).send({"message": "error passwordfile"});
                    }
                })
            }

        } else {
            if(typeof password ==='undefined'){
                return true
            } else {
                const passwords = path.resolve(__dirname, 'filePassword.json')
                let data = fs.readFileSync(passwords)
                let contentObj = JSON.parse(data)
                contentObj[filename] = password
                const modiObj = JSON.stringify(contentObj)
                fs.writeFile(passwords, modiObj, (err) => {
                    if (err) {
                        res.status(400).send({"message": "error passwordfile"});
                    }
                })
            }
        }
        return true
    }
}
function checkPassword(tempName,password){
    const passwords = path.resolve(__dirname,'filePassword.json')
    const temp = fs.readFileSync(passwords)
        const obj = JSON.parse(temp)
        let check = false
        let arrFileName = []
    Object.keys(obj).forEach((key)=>{
            let objPassword =obj[key].toLowerCase()
            if(key===tempName && objPassword===password){
                check=true
            }
            arrFileName.push(key)
        })

    if(!arrFileName.includes(tempName)){
            check=true
        }
        return check
}
function  deletePassword(tempName){
    const passwords = path.resolve(__dirname,'filePassword.json')
    const temp = fs.readFileSync(passwords)
    const obj = JSON.parse(temp)
    let newObj = {}

    Object.keys(obj).forEach((key)=>{
        if(key!==tempName ){
            newObj[key]=obj[key]
        }
    })
    let content = JSON.stringify(newObj)
    fs.writeFile(passwords, content, (err) => {
        if (err) {
            res.status(400).send({"message": "Please specify 'content' parameter"});
        }
    })
}

function createFile(req, res, next) {
    if(typeof  req.body.filename === 'undefined'){
        res.status(400).send({"message": "Server error"});
    }
    let {filename, content,password} = req.body
    const fileExt = ['log', 'txt', 'json', 'yaml', 'xml', 'js']
    const ext = path.extname(filename).substr(1)
    const tempName = path.resolve(__dirname,'files',filename)
    const dir = './files';

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    try {

        if (!fileExt.includes(ext)) {
            res.status(400).send({"message": "Please specify 'filename' parameter"});
            return
        }
        if (content.trim()==='') {
            res.status(400).send({"message": "Please specify 'content' parameter"});
           return
        }

        let fileExists = fs.existsSync(tempName)
        if(fileExists){
          //  res.status(400).send({"message": "File already exist"});
        } else {
            let check = createPasswordFile(res, filename, password);
            if (!check) {
                res.status(400).send({"message": "password can't be empty"});
               return
            }
            fs.writeFile(tempName, content, (err) => {
                if (err) {
                    res.status(400).send({"message": "Please specify 'content' parameter"});
                   return

                }
            })
            fs.stat(tempName, (error, stats) => {
                const timeStamp = stats.birthtime
                res.status(200).send({
                    "message": "Success",
                    "filename": filename,
                    "content": content,
                    "extension": ext,
                    "uploadedDate": timeStamp
                });
            })
        }
    } catch (e) {

        res.status(500).send({"message":"Server error"});

    }
}

function getFiles(req, res, next) {

    const filesDir = path.resolve(__dirname,'files')
    try {
        fs.readdir(filesDir, function (err, files) {
            if (err) {
                res.status(400).send({"message": err.message()});
               return
            }
            let result = []
            files.forEach(function (file) {
                result.push(file)
            });
            res.status(200).send({
                "message": "Success",
                "files": result
            });
        });
    } catch (e){
        res.status(500).send({"message":"Server error"});


    }
}

const getFile = (req, res, next) => {
    try {
        const tempName = req.params.filename
        const password = req.query.password
        const fileName= path.resolve(__dirname,'files',tempName)
        let fileExists = fs.existsSync(fileName)
        if(fileExists){
            let check = checkPassword(tempName,password)
            if(!check){
                // res.status(400).send({"message": "No correct password"});
               // return
            } else {
                fs.stat(fileName, (error, stats) => {
                    const timeStamp = stats.birthtime
                    const ext = path.extname(fileName).substr(1)
                    fs.readFile(fileName, {encoding: 'utf-8'}, (err, data) => {
                        if (err) {
                            res.status(400).send({"message": err.message()});
                        }
                        res.status(200).send({
                            "message": "Success",
                            "filename": tempName,
                            "content": data,
                            "extension": ext,
                            "uploadedDate": timeStamp
                        });
                    })

                })
            }
        } else {
            res.status(400).send({"message": "Not found"});
           return
        }
    } catch (e){
        res.status(500).send({"message":"Server error"});
    }

}



function putFile(req, res, next)  {
    try {
        const tempName = req.params.filename
        const password = req.query.password

        const {content} = req.body
        if (content.trim()==='') {
            res.status(400).send({"message": "Please specify 'content' parameter"});
           return
        }
        const fileName= path.resolve(__dirname,'files',tempName)
        let fileExists = fs.existsSync(fileName)
        if(fileExists){
            let check = checkPassword(tempName,password)
            if(!check){
                res.status(400).send({"message": "No correct password"});
               return
            } else {
                fs.writeFile(fileName, content, (err) => {
                    if (err) {
                        res.status(400).send({"message": "No correct content"});
                       return
                    }
                })
                fs.stat(fileName, (error, stats) => {
                    const timeStamp = stats.birthtime
                    const ext = path.extname(fileName).substr(1)
                    fs.readFile(fileName, {encoding: 'utf-8'}, (err, data) => {
                        if (err) {
                            res.status(400).send({"message": err.message()});
                           return
                        }
                        res.status(200).send({
                            "message": "Success",
                            "filename": tempName,
                            "content": data,
                            "extension": ext,
                            "uploadedDate": timeStamp
                        });
                    })

                })
            }
        } else {
            res.status(400).send({"message": "File doesn't exist"});
           return
        }
    } catch (e){
        res.status(500).send({"message":"Server error"});

    }

}


function deleteFile(req, res, next)  {

    try {
        const tempName = req.params.filename
        const password = req.query.password

        const fileName = path.resolve(__dirname, 'files', tempName)
        let fileExists = fs.existsSync(fileName)
        if (fileExists) {
            let check = checkPassword(tempName,password)
            if(!check){
                res.status(400).send({"message": "No corect password"});
               return
            } else {
                fs.unlink(fileName, (err) => {
                    if (err) {
                        res.status(400).send({"message": "No deleted"});
                       return
                    }
                    deletePassword(tempName)
                    res.status(200).send({"message": "File deleted"});
                })
            }
        } else{
            res.status(400).send({"message": "File doesn't exist"});
           return
        }
    } catch (e){
        res.status(500).send({"message":"Server error"});

    }
}

module.exports = {
    createFile,
    getFiles,
    getFile,
    putFile,
    deleteFile
}
